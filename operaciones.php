<?php
//importamos spreadsheet para poder generar ficheros "excel"
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet(); 
/*empezamos creando la conexión a nuestra base de datos*/
$servername = "localhost";
$username = "david";
$password = "pass";
$dbname = "prueba";

$conn = new mysqli($servername, $username, $password, $dbname);

//Si la conexión falla mostramos un mensaje de error y si no, seguimos adelante
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
else{
  //definimos una variable vacia a la que le pasaremos el valor del elemento que enviamos desde el formulario
  //en caso de que dicho elemento exista y tenga algún contenido. Debemos comprobar si existe al menos el elemento
  //antes de asignarle su valor ya que de no existir, el valor de nuestra variable sería null.
  $prueba="";
  //comprobamos si el elemento existe con la función isset(). También comprobamos si tiene algún valor para imprimir su
  //contenido o no. Como el método de envió del formulario es post, con $_POST['<name del elemento>'] debemos recoger
  //el valor del elemento
  if(isset($_POST['prueba']) && $_POST['prueba']!=""){
      //si el elemento existe y tiene contenido lo imprimimos
      $prueba=$_POST['prueba'];
      echo "El valor enviado desde el formulario es: ".$prueba."<br><br>";
  //de no ser así imprimimos un mensaje de error
  }else echo "No se envió nada desde el formulario...<br>";
  //P.D.: en este caso el valor del elemento recogido nunca va a estar vacio porque ya estamos evitándolo mediante
  //la función enviar() de javascript definida en form.html. Por otro lado también sabemos seguro que el elemento
  //existe pero es importante hacer estas comprobaciones al hacer aplicaciones para que no rompan.

  //Ahora guardamos el texto en un registro de nuestra tabla de nuestra bd
  $sql = "INSERT INTO prueba (texto)
  VALUES ('$prueba')";
  //Mostramos un mensaje para saber si la inserción a tenido éxito o no
  if ($conn->query($sql) === TRUE) {
    //echo "Nuevo registro creado con éxito.<br>";
  } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
    echo "<br>";
  }
  //Ahora leemos todos los registros de nuestra tabla
  $sql="SELECT * FROM prueba";
  if($result = mysqli_query($conn, $sql)){
    //echo "Lectura de la tabla realizada con éxito.<br>";
    //Guardamos el resultado de la lectura en una variable
    $result = $conn->query($sql);
    //Comprobamos si hay registros en la tabla y mostramos el resultado
    if ($result->num_rows > 0) {

      //Inicializamos array para el xml
      //$xmlFile=array();

      $i=1;
      echo "**Registros guardados**********************************************************<br>";
      while($row = $result->fetch_assoc()) {
        echo "id: " . $row["id"]. " - Texto: " . $row["texto"]. "<br>";

        //Rellenamos el array con los registros de la tabla
        //$xlsxFile[$row["id"]]=$row["texto"];

        //Agregando valores al excel
        $sheet->setCellValue('A'.$i, $row["id"]); 
        $sheet->setCellValue('B'.$i, $row["texto"]); 
        $i++;
      }
      echo "*******************************************************************************<br>";
      //Escribimos el excel
      $writer = new Xlsx($spreadsheet); 
      //Guardamos el excel
      $writer->save('registro.xlsx');
    } else {
      echo "No hay registros en la tabla";
    }
  } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
    echo "<br>";
  }
  echo "<a href='index.php'>Volver</a><br>";
  //Cerramos la conexión
  $conn->close();
}
?>