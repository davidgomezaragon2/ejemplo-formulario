<html lang="spanish">

    <head>
        <!--cabecera para que se reconozcan caracteres especiales como acentos,ñ,etc-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!--script de javascript que evalúa si estamos enviando contenido a través del elemento con id prueba
        de nuestro formulario-->
        <script type="text/javascript">
            function enviar(){
                //recogemos el valor de nuestro elemento prueba a través del id y lo metemos en una variable
                var texto=document.getElementById("prueba").value;
                if(texto==""){
                    //si el elemento no tiene un valor nos dará un mensaje de error
                    alert("Debes introducir texto");
                //en caso contrario enviamos el formulario
                }else document.getElementById("formulario").submit();
            }
        </script>
    </head>
    <body>
        <!--method define cómo vamos a enviar los elementos de nuestro formulario y action señala la página
        de destino-->
        <form method="post" action="operaciones.php" id="formulario">
            Mete aquí cualquier cosa <input type="text" name="prueba" id="prueba">
            <!--onclick es un atributo disparador de eventos que sirve para llamar a una función cuando
            hagamos click sobre dicho elemento-->
            <input type="button" id="envio" value="Enviar" onclick="enviar()"><br>
        </form>
        <?php
        if (file_exists("registro.xlsx")) {
            echo "<p><a href='registro.xlsx'>Descargar registro</a></p>";
        }
        ?>
    </body>

</html>