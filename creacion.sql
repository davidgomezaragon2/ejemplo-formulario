/*borramos bd si existe*/
DROP DATABASE IF EXISTS prueba;
/*creamos bd*/
CREATE DATABASE prueba;
/*Debemos indicar la bd a usar antes de hacer algo con ella*/
USE prueba;
/*Creamos la tabla prueba en la bd prueba*/
CREATE TABLE prueba (
id INT(3) AUTO_INCREMENT PRIMARY KEY,
texto VARCHAR(100) NOT NULL
);
/*Creamos un nuevo usuario para no usar root y le damos permisos*/
/*Por si acaso en en phpMyAdmin (se entra aquí pulsando admin en el módulo de mysql en xampp), en la pestaña cuentas de usuarios
le hemos dado todos los permisos a esta nueva cuenta*/
CREATE USER 'david'@'localhost' IDENTIFIED BY 'pass';
GRANT ALL PRIVILEGES ON prueba.* TO 'david'@'localhost';
